#include <stdio.h>

int attaccoorizzontaledueriga1caso1(char m[10][10])
{
    int j;
    for(j=1; j<7; j++)
    {        if(m[0][j]==1 && m[0][j+1]==1 && m[0][j-1]==0 && m[0][j+2]==0 && m[0][j+3]==0)
             return j+2;}
    return 10;
}

int attaccoorizzontaleduecaso1(char m[10][10])
{
    int i, j;
    for(i=1; i<10; i++)
    for(j=1; j<7; j++)
    {        if(m[i][j]==1 && m[i][j+1]==1 && m[i][j-1]==0 && m[i][j+2]==0 && m[i][j+3]==0 && m[i-1][j-1]!=0 && m[i-1][j+2]!=0 && m[i-1][j+3]!=0)
             return j+2;}
    return 10;
}

int attaccoorizzontaledueriga1caso2(char m[10][10])
{
    int j;
    for(j=2; j<8; j++)
    {        if(m[0][j]==1 && m[0][j+1]==1 && m[0][j-1]==0 && m[0][j-2]==0 && m[0][j+2]==0)
             return j-1;}
    return 10;
}

int attaccoorizzontaleduecaso2(char m[10][10])
{
    int i, j;
    for(i=1; i<10; i++)
    for(j=2; j<8; j++)
    {        if(m[i][j]==1 && m[i][j+1]==1 && m[i][j-1]==0 && m[i][j-2]==0 && m[i][j+2]==0 && m[i-1][j-1]!=0 && m[i-1][j-2]!=0 && m[i-1][j+2]!=0)
             return j-1;}
    return 10;
}

int attaccoorizzontaledueriga1caso3(char m[10][10])
{
    int j;
    for(j=1; j<7; j++)
    {        if(m[0][j]==1 && m[0][j+2]==1 && m[0][j-1]==0 && m[0][j+1]==0 && m[0][j+3]==0)
             return j+1;}
    return 10;
}   

int attaccoorizzontaleduecaso3(char m[10][10])
{
    int i, j;
    for(i=1; i<10; i++)
    for(j=2; j<7; j++)
    {        if(m[i][j]==1 && m[i][j+2]==1 && m[i][j-1]==0 && m[i][j+1]==0 && m[i][j+3]==0 && m[i-1][j-1]!=0 && m[i-1][j+1]!=0 && m[i-1][j+3]!=0)
             return j+1;}
    return 10;
}
