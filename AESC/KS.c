#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "oAES.h"

void CreationRcon(int ncol, int nk, int nr, int R[ncol*(nr+1)/nk + 1]){
	int i;
	R[0] = 1;
	for(i=1; i<ncol*(nr+1)/nk+1; i++){
		R[i] = mGF256(2,R[i-1]);
	}
}

void SubByte(int W[4]){
	int i;
	for(i=0; i<4; i++){	
	//printf("\n SubByte --- %d ", W[i]);
	W[i] = SBox(W[i]); 
	//printf("%d and this", W[i]);
	}
}

void RotByte(int W[4]){
	int temp=W[0];
	W[0]=W[1]; W[1]=W[2];
	W[2]=W[3]; W[3]=temp;
}

void KeyExpansion(int ncol, int nk, int nr, int key[4*nk], int  W[ncol*(nr+1)][4]){
	int R[ncol*(nr+1)/nk + 1], temp[4];
	
	//printf("\n --- KE1 ---");
	
	int i, j;
	CreationRcon(ncol, nk, nr, R);
	for(i=0; i<nk; i++){ 	for(j=0; j<4; j++){
		W[i][j] = key[4*i+j];
		//printf("%d ", W[i][j]);
	}}
	
	//printf("\n --- KE2 ---");
	
	if(nk <= 6){
		for(i=nk; i<ncol*(nr+1); i++){
			for(j=0; j<4; j++){
				temp[j] = W[i-1][j];
				//printf("%d ", temp[j]);
			}
			//printf("\n --- KE3 --- %d", i);
			if(i%nk == 0){
				//printf("\n ---> (1)"); for(j=0; j<4; j++){ printf("%d ", temp[j]); }
				RotByte(temp);
				//printf("\n ---> (2)"); for(j=0; j<4; j++){ printf("%d ", temp[j]); }
				SubByte(temp);
				//printf("\n ---> (3)"); for(j=0; j<4; j++){ printf("%d ", temp[j]); }
				temp[0] ^= R[i/nk];
				//printf("\n ---> (4)"); for(j=0; j<4; j++){ printf("%d ", temp[j]); }
			}
			for(j=0; j<4; j++){
				W[i][j] = W[i-nk][j] ^ temp[j];
			}
		}
	}else{
		for(i=nk; i<ncol*(nr+1); i++){
			for(j=0; j<4; j++){
				temp[j] = W[i-1][j];
			}
			if(i%nk == 0){
				RotByte(temp);
				SubByte(temp);
				temp[0] ^= R[i/nk];
			}else if(i%nk == 4){
				SubByte(temp);
			}
			for(j=0; j<4; j++){
				W[i][j] = W[i-nk][j] ^ temp[j];
			}
		}
	}
	
	/*for(i=0; i<ncol*(nr+1); i++){
		printf("\n");
		for(j=0; j<4; j++){
				printf("%d ", W[i][j]);
		}
	}*/
	
	/*printf("\n W \n");
	for(i=0; i<ncol*(nr+1); i++){
		printf("-----");
	for(j=0; j<4; j++){
		printf("%d ", W[i][j]);
	}}*/
}

void CreationRK(int ncol, int nk, int nr, int  W[ncol*(nr+1)][4], int RK[nr+1][4][ncol]){
	int i, j, k;
	for(i=0; i<nr+1; i++){
	for(j=0; j<ncol; j++){
	for(k=0; k < 4;  k++){
		RK[i][k][j] = W[i*ncol+j][k];
	}}}
	
	/*for(i=0; i<nr+1; i++){
	for(j=0; j<ncol; j++){
	for(k=0; k < 4;  k++){
		printf("%d ", RK[i][k][j]);
	} printf("\n"); } printf("\n\n");}*/
}

void KeySchedule(int ncol, int nk, int nr, char *s, int RK[nr+1][4][ncol]){
	int key[4*nk], W[ncol*(nr+1)][4];
	
	//printf("\n --- KS1 ---");
	int k = strlen(s), i;
	for(i=0; i<4*nk; i++){
		if(i<k){ key[i] = s[i];	}
		else{ key[i] = 0; }
	}
	//printf("\n --- KS2 ---");
	KeyExpansion(ncol, nk, nr, key, W);
	//printf("\n --- KS3 ---");
	CreationRK(ncol, nk, nr, W, RK);
	//printf("\n --- KS4 ---");
	//printf("CIAO!!!! %d", RK[0][0][0]);
	
	
}
