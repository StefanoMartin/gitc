#include <stdio.h>

int cercavittoriaorizzontalediagonale1caso1(char m[10][10])
{int i,j;
for(j=2; j<8; j++)
for(i=0; i<7; i++)
{
         if(m[i][j]==1 && m[i][j+1]==1 && m[i+1][j]==1 && m[i+2][j+1]==1 && m[i][j-1]==0 && m[i][j-2]==0 && m[i+3][j+2]==0 && m[i+2][j+2]!=0)
         return j-1;
}return 10;}

int cercavittoriaorizzontalediagonale1caso2(char m[10][10])
{int i,j;
for(j=3; j<8; j++)
for(i=0; i<7; i++)
{
         if(m[i][j]==1 && m[i][j-2]==1 && m[i+1][j]==1 && m[i+2][j+1]==1 && m[i][j-1]==0 && m[i][j-3]==0 && m[i+3][j+2]==0 && m[i+2][j+2]!=0)
         return j-1;
}return 10;}

int cercavittoriaorizzontalediagonale1caso3(char m[10][10])
{int i,j;
for(j=1; j<4; j++)
for(i=0; i<7; i++)
{
         
         if(m[i+2][j]==1 && m[i+1][j+1]==1 && m[i][j+3]==1 && m[i][j+4]==1 && m[i+3][j-1]==0 && m[i][j+2]==0 && m[i][j+5]==0 && m[i+2][j-1]!=0)
         return j+2;
}return 10;}

int cercavittoriaorizzontalediagonale1caso4(char m[10][10])
{int i,j;
for(j=2; j<9; j++)
for(i=1; i<8; i++)
{
         if(m[i][j]==1 && m[i][j+1]==1 && m[i+1][j]==1 && m[i+1][j+2]==1 && m[i][j-1]==0 && m[i-1][j-2]==0 && m[i-1][j-1]!=0 && (i==1 ? 5==5 : m[i-2][j-2]!=0))
         return j-1;
}return 10;}

int cercavittoriaorizzontalealtradiagonale1caso1(char m[10][10])
{int i,j;
for(j=1; j<7; j++)
for(i=0; i<7; i++)
{
         if(m[i][j]==1 && m[i+2][j]==1 && m[i][j+1]==1 && m[i+1][j+1]==1 && m[i+3][j-1]==0 && m[j+2][i]==0 && m[j+3][i]==0 && m[i+2][j-1]!=0)
         return j+2;
}return 10;}

int cercavittoriaorizzontalealtradiagonale1caso2(char m[10][10])
{int i,j;
for(j=2; j<7; j++)
for(i=0; i<7; i++)
{
         if(m[i][j]==1 && m[i+2][j-1]==1 && m[i+1][j]==1 && m[i][j+2]==1 && m[i+3][j-2]==0 && m[j+1][i]==0 && m[j+3][i]==0 && m[i+2][j-2]!=0)
         return j+1;
}return 10;}

int cercavittoriaorizzontalealtradiagonale1caso3(char m[10][10])
{int i,j;
for(j=1; j<4; j++)
for(i=0; i<7; i++)
{
         if(m[i][j]==1 && m[i][j+1]==1 && m[i+1][j+3]==1 && m[i+2][j+4]==1 && m[i+3][j+5]==0 && m[j+2][i]==0 && m[j-1][i]==0 && m[i+2][j+5]!=0)
         return j+2;
}return 10;}

int cercavittoriaorizzontalealtradiagonale1caso4(char m[10][10])
{int i,j;
for(j=0; j<7; j++)
for(i=1; i<8; i++)
{
         if(m[i][j]==1 && m[i][j+1]==1 && m[i+2][j]==1 && m[i+1][j+1]==1 && m[i][j+2]==0 && m[i-1][j+3]==0 && m[i-1][j+2]!=0 && (i==1 ? 1==1 : m[i-2][j+3]!=0))
         return j+2;
}return 10;}

int cercavittoraorizzontalediagonale2caso1(char m[10][10])
{int i,j;
for(j=2; j<7; j++)
for(i=1; i<8; i++)
{
         if(m[i][j]==1 && m[i-1][j]==1 && m[i][j-1]==1 && m[i+1][j+2]==1 && m[i][j-2]==0 && m[i][j+1]==0 && m[i+2][j+3]==0 && m[i-1][j-2]!=0 && m[i-1][j+1]!=0 && m[i+1][j+3]!=0)
         return j+1;
}return 10;}

int cercavittoraorizzontalediagonale2caso2(char m[10][10])
{int i,j;
for(j=1; j<7; j++)
for(i=1; i<8; i++)
{
         if(m[i][j]==1 && m[i-1][j]==1 && m[i][j+2]==1 && m[i+1][j+2]==1 && m[i][j-1]==0 && m[i][j+1]==0 && m[i+2][j+3]==0 && m[i-1][j-1]!=0 && m[i-1][j+1]!=0 && m[i+1][j+3]!=0)
         return j+1;
}return 10;}

int cercavittoraorizzontalediagonale2caso3(char m[10][10])
{int i,j;
for(j=0; j<7; j++)
for(i=1; i<8; i++)
{
         if(m[i-1][j]==1 && m[i][j+2]==1 && m[i][j+3]==1 && m[i+1][j+2]==1 && m[i][j]==0 && m[i][j+1]==0 && m[i+2][j+3]==0 && m[i-1][j+1]!=0 && m[i+1][j+3]!=0)
         return j+1;
}return 10;}

int cercavittoraorizzontalealtradiagonale2caso1(char m[10][10])
{int i,j;
for(j=3; j<8; j++)
for(i=1; i<8; i++)
{
         if(m[i][j]==1 && m[i-1][j]==1 && m[i][j+1]==1 && m[i+1][j-2]==1 && m[i][j+2]==0 && m[i][j-1]==0 && m[i+2][j-3]==0 && m[i-1][j+2]!=0 && m[i-1][j-1]!=0 && m[i+1][j-3]!=0)
         return j-1;
}return 10;}

int cercavittoraorizzontalealtradiagonale2caso2(char m[10][10])
{int i,j;
for(j=3; j<9; j++)
for(i=1; i<8; i++)
{
         if(m[i][j]==1 && m[i-1][j]==1 && m[i][j-2]==1 && m[i+1][j-2]==1 && m[i][j+1]==0 && m[i][j-1]==0 && m[i+2][j-3]==0 && m[i-1][j+1]!=0 && m[i-1][j-1]!=0 && m[i+1][j-3]!=0)
         return j-1;
}return 10;}

int cercavittoraorizzontalealtradiagonale2caso3(char m[10][10])
{int i,j;
for(j=3; j<10; j++)
for(i=1; i<8; i++)
{
         if(m[i-1][j]==1 && m[i][j-2]==1 && m[i][j-3]==1 && m[i+1][j-2]==1 && m[i][j]==0 && m[i][j-1]==0 && m[i+2][j-3]==0 && m[i-1][j-1]!=0 && m[i+1][j-3]!=0)
         return j-1;
}return 10;}

int cercavittoriaorizzontalediagonale3caso1(char m[10][10])
{int i,j;
for(j=2; j<8; j++)
for(i=1; i<8; i++)
{
         if(m[i][j]==1 && m[i+1][j]==1 && m[i-1][j-1]==1 && m[i+1][j-1]==1 && m[i+1][j+1]==0 && m[i+1][j-2]==0 && m[i+2][j+2]==0 &&  m[i+1][j+2]!=0 && m[i][j-2]!=0 && m[i][j+1]!=0)
         return j+1;
}return 10;}

int cercavittoriaorizzontalediagonale3caso2(char m[10][10])
{int i,j;
for(j=1; j<8; j++)
for(i=1; i<9; i++)
{
         if(m[i][j]==1 && m[i+1][j]==1 && m[i-1][j-1]==1 && m[i+1][j-1]==1 && m[i+1][j+1]==0 && m[i+1][j+2]==0 && m[i][j+2]!=0 && m[i][j+1]!=0)
         return j+1;
}return 10;}

int cercavittoriaorizzontalediagonale3caso3(char m[10][10])
{int i,j;
for(j=1; j<8; j++)
for(i=1; i<8; i++)
{
         if(m[i][j]==1 && m[i+1][j]==1 && m[i-1][j-1]==1 && m[i+1][j+2]==1 && m[i+1][j+1]==0 && m[i+2][j+2]==0 && m[i+1][j-1]==0 && m[i][j-1]!=0 && m[i][j+1]!=0)
         return j+1;
}return 10;}

int cercavittoriaorizzontalediagonale3caso4(char m[10][10])
{int i,j;
for(j=1; j<7; j++)
for(i=1; i<8; i++)
{
         if(m[i][j]==1 && m[i+1][j]==1 && m[i-1][j-1]==1 && m[i+1][j+2]==1 && m[i+1][j+1]==0 && m[i+2][j+2]==0 && m[i+1][j+3]==0 && m[i][j+3]!=0 && m[i][j+1]!=0)
         return j+1;
}return 10;}

int cercavittoriaorizzontalediagonale3caso5(char m[10][10])
{int i,j;
for(j=1; j<7; j++)
for(i=1; i<8; i++)
{
         if(m[i][j]==1 && m[i+1][j+3]==1 && m[i-1][j-1]==1 && m[i+1][j+2]==1 && m[i+1][j+1]==0 && m[i+2][j+2]==0 && m[i+1][j]==0 && m[i][j+1]!=0)
         return j+1;
}return 10;}

int cercavittoriaorizzontalediagonale3caso6(char m[10][10])
{int i,j;
for(j=1; j<6; j++)
for(i=1; i<9; i++)
{
         if(m[i][j]==1 && m[i+1][j+3]==1 && m[i-1][j-1]==1 && m[i+1][j+2]==1 && m[i+1][j+1]==0 && m[i+2][j+2]==0 && m[i+1][j+4]==0 && m[i][j+1]!=0 && m[i][j+4]!=0)
         return j+1;
}return 10;}

int cercavittoriaorizzontalealtradiagonale3caso1(char m[10][10])
{int i,j;
for(j=2; j<8; j++)
for(i=1; i<8; i++)
{
         if(m[i][j]==1 && m[i+1][j]==1 && m[i-1][j+1]==1 && m[i+1][j+1]==1 && m[i+1][j-1]==0 && m[i+1][j+2]==0 && m[i+2][j-2]==0 &&  m[i+1][j-2]!=0 && m[i][j+2]!=0 && m[i][j-1]!=0)
         return j-1;
}return 10;}

int cercavittoriaorizzontalealtradiagonale3caso2(char m[10][10])
{int i,j;
for(j=2; j<9; j++)
for(i=1; i<9; i++)
{
         if(m[i][j]==1 && m[i+1][j]==1 && m[i-1][j+1]==1 && m[i+1][j+1]==1 && m[i+1][j-1]==0 && m[i+1][j-2]==0 && m[i][j-2]!=0 && m[i][j-1]!=0)
         return j-1;
}return 10;}

int cercavittoriaorizzontalealtradiagonale3caso3(char m[10][10])
{int i,j;
for(j=2; j<9; j++)
for(i=1; i<8; i++)
{
         if(m[i][j]==1 && m[i+1][j]==1 && m[i-1][j+1]==1 && m[i+1][j-2]==1 && m[i+1][j-1]==0 && m[i+2][j-2]==0 && m[i+1][j+1]==0 && m[i][j+1]!=0 && m[i][j-1]!=0)
         return j-1;
}return 10;}

int cercavittoriaorizzontalealtradiagonale3caso4(char m[10][10])
{int i,j;
for(j=3; j<9; j++)
for(i=1; i<8; i++)
{
         if(m[i][j]==1 && m[i+1][j]==1 && m[i-1][j+1]==1 && m[i+1][j-2]==1 && m[i+1][j-1]==0 && m[i+2][j-2]==0 && m[i+1][j-3]==0 && m[i][j-3]!=0 && m[i][j-1]!=0)
         return j-1;
}return 10;}

int cercavittoriaorizzontalealtradiagonale3caso5(char m[10][10])
{int i,j;
for(j=3; j<9; j++)
for(i=1; i<8; i++)
{
         if(m[i][j]==1 && m[i+1][j-3]==1 && m[i-1][j+1]==1 && m[i+1][j-2]==1 && m[i+1][j-1]==0 && m[i+2][j-2]==0 && m[i+1][j]==0 && m[i][j-1]!=0)
         return j-1;
}return 10;}

int cercavittoriaorizzontalealtradiagonale3caso6(char m[10][10])
{int i,j;
for(j=4; j<9; j++)
for(i=1; i<9; i++)
{
         if(m[i][j]==1 && m[i+1][j-3]==1 && m[i-1][j+1]==1 && m[i+1][j-2]==1 && m[i+1][j-1]==0 && m[i+2][j-2]==0 && m[i+1][j-4]==0 && m[i][j-1]!=0 && m[i][j-4]!=0)
         return j-1;
}return 10;}








