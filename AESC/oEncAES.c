#include <stdlib.h>
#include "oAES.h"
#include "ioAES.h"
#include "KS.h"

/*void printForme(int ncol, int A[4][ncol]){
	int i,j;
	printf("\n");
	for(i=0; i<4; i++){ for(j=0; j<ncol; j++){
		printf("%d ", A[i][j]);
	}	printf("\n");	}
}

void printForme2(int ncol, int nr, int RK[nr+1][4][ncol], int r){
	int i,j;
	printf("\n");
	for(i=0; i<4; i++){ for(j=0; j<ncol; j++){
		printf("%d ", RK[r][i][j]);
	}	printf("\n");	}
}*/

void encRound(int ncol, int nr, int A[4][ncol], int RK[nr+1][4][ncol], int i){
	//printf("\n--- ERR 1 ---");
	ByteSub(ncol, A); //printForme(ncol, A); printf("\n--- ERR 2 ---"); 
	ShiftRows(ncol, A);// printForme(ncol, A); printf("\n--- ERR 3 ---");
	MixColumn(ncol, A); //printForme(ncol, A); printf("\n--- ERR 4 ---");
	AddRoundKey(ncol, nr, A, RK, i); 
	 //printForme2(ncol, nr, RK, i); printForme(ncol, A); printf("\n--- ERR 5 ---");
}

void encFinRound(int ncol, int nr, int A[4][ncol], int RK[nr+1][4][ncol], int i){
	ByteSub(ncol, A);
	ShiftRows(ncol, A);
	AddRoundKey(ncol, nr, A, RK, i);
}

void decRound(int ncol, int nr, int A[4][ncol], int RK[nr+1][4][ncol], int i){
	AddRoundKey(ncol, nr, A, RK, i);
	IMixColumn(ncol, A);
	IShiftRows(ncol, A);
	IByteSub(ncol, A);	
}

void dec1stRound(int ncol, int nr, int A[4][ncol], int RK[nr+1][4][ncol], int i){
	AddRoundKey(ncol, nr, A, RK, i);
	IShiftRows(ncol, A);
	IByteSub(ncol, A);	
}

void encRij(int ncol, int nr, int A[4][ncol], int RK[nr+1][4][ncol]){
	int i;
//	printf("\n--- ER 1 ---"); printForme(ncol, A);
	AddRoundKey(ncol, nr, A, RK, 0); //printForme(ncol, A); printForme2(ncol, nr, RK, 0);
	//printf("\n--- ER 2 ---");
	for(i=1; i<nr; i++){	encRound(ncol, nr, A, RK, i);  } //printf("\n--- ER 3 --- %d", i);
	//printf("\n--- ER 4 ---");
	encFinRound(ncol, nr, A, RK, nr);
}

void decRij(int ncol, int nr, int A[4][ncol], int RK[nr+1][4][ncol]){
	int i;
	dec1stRound(ncol, nr, A, RK, nr);
	for(i=nr-1; i>0; i--){	decRound(ncol, nr, A, RK, i); }
	AddRoundKey(ncol, nr, A, RK, 0);
}

