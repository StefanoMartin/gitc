This Java folder is for my projects in Java.

Currently they are two project available.

**AES Project:** it is an (almost) concluded project. This program permits to encrypt and decrypt any files in AES-128, AES-192 and AES-256. You can use it by terminal. Be careful to not use it in illegal or improper way. A working executable exe file is available from the following link: http://www.yeyedo.com/CFiles/AES.exe

**Connect four:** i it is a concluded project created during my bachelor. It was translated few days ago in English (although the code is still "Italian-friendly"). Few bugs were corrected too. This program permits to play "Connect four" against another human player or against a medium level CPU. A history of your matches will be saved after any match. A working executable exe file is available from the following link: http://www.yeyedo.com/CFiles/forzaquattro.exe

Both the projects are managed by Stefano Martin