#ifndef CHECK
#define CHECK
#include "GF256.c"

int mGF256(int a, int b);
int iGF256(int a);
void pM(int a[4], int b[4], int* d);

#endif
