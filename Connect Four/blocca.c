#include <stdio.h>

int bloccaorizzontaleriga1caso1(char m[10][10])
{
    int j;
   
    for(j=0; j<7; j++)
    {       
             if(m[0][j]==2 && m[0][j+1]==2 && m[0][j+2]==2 && m[0][j+3]==0)
             return j+3;}
    return 10;
}

int bloccaorizzontaleriga1caso2(char m[10][10])
{
    int j;
    
    for(j=1; j<8; j++)
    {        
             if(m[0][j]==2 && m[0][j+1]==2 && m[0][j+2]==2 && m[0][j-1]==0)
             return j-1;}
    return 10;
}

int bloccaorizzontaleriga1caso34(char m[10][10])
{
    int j;
    
    for(j=0; j<7; j++)
    {        
             if(m[0][j]==2 && m[0][j+1]==2 && m[0][j+3]==2 && m[0][j+2]==0)
             return j+2;
             if(m[0][j]==2 && m[0][j+2]==2 && m[0][j+3]==2 && m[0][j+1]==0)
             return j+1;}
    return 10;
}

int bloccaorizzontalecaso1(char m[10][10])
{
    int i, j;
    
    for(i=1; i<10; i++)
    for(j=0; j<7; j++)
    {        
             if(m[i][j]==2 && m[i][j+1]==2 && m[i][j+2]==2 && m[i][j+3]==0 && m[i-1][j+3]!=0)
             return j+3;}
    return 10;
}

int bloccaorizzontalecaso2(char m[10][10])
{
    int i, j;
    
    for(i=1; i<10; i++)
    for(j=1; j<8; j++)
    {        
             if(m[i][j]==2 && m[i][j+1]==2 && m[i][j+2]==2 && m[i][j-1]==0 && m[i-1][j-1]!=0)
             return j-1;}
    return 10;
}

int bloccaorizzontalecaso34(char m[10][10])
{
    int i, j;
    
    for(i=1; i<10; i++)
    for(j=0; j<7; j++)
    {        
             if(m[i][j]==2 && m[i][j+1]==2 && m[i][j+3]==2 && m[i][j+2]==0 && m[i-1][j+2]!=0)
             return j+2;
             if(m[i][j]==2 && m[i][j+2]==2 && m[i][j+3]==2 && m[i][j+1]==0 && m[i-1][j+1]!=0)
             return j+1;}
    return 10;
}

int bloccaverticale(char m[10][10])
{
    int i, j;
    
    for(i=0; i<7; i++)
    for(j=0; j<10; j++)
    {        
             if(m[i][j]==2 && m[i+1][j]==2 && m[i+2][j]==2 && m[i+3][j]==0)
             return j;}
    return 10;
}

int bloccadiagonalecaso1(char m[10][10])
{
    int i, j;
    
    for(i=0; i<7; i++)
    for(j=0; j<7; j++)
    {        ;
             if(m[i][j]==2 && m[i+1][j+1]==2 && m[i+2][j+2]==2 && m[i+3][j+3]==0 && m[i+2][j+3]!=0)
             return j+3;}
    return 10;
}

int bloccadiagonalecaso2riga1(char m[10][10])
{
    int j;
    
    for(j=1; j<8; j++)
    {        
             if(m[1][j]==2 && m[2][j+1]==2 && m[3][j+2]==2 && m[0][j-1]==0)
             return j-1;}
    return 10;
}

int bloccadiagonalecaso2(char m[10][10])
{
    int i, j;
    
    for(i=2; i<7; i++)
    for(j=1; j<7; j++)
    {        
             if(m[i][j]==2 && m[i+1][j+1]==2 && m[i+2][j+2]==2 && m[i-1][j-1]==0 && m[i-2][j-1]!=0)
             return j-1;}
    return 10;
}

int bloccadiagonalecaso34(char m[10][10])
{
    int i, j;
    
    for(i=0; i<7; i++)
    for(j=0; j<7; j++)
    {        
             if(m[i][j]==2 && m[i+1][j+1]==2 && m[i+3][j+3]==2 && m[i+2][j+2]==0 && m[i+1][j+2]!=0)
             return j+2;
             if(m[i][j]==2 && m[i+2][j+2]==2 && m[i+3][j+3]==2 && m[i+1][j+1]==0 && m[i][j+1]!=0)
             return j+1;}
    return 10;
}
             
int bloccaaltradiagonalecaso1riga1(char m[10][10])
{
    int j;
    
    for(j=0; j<7; j++)
    {        
             if(m[3][j]==2 && m[2][j+1]==2 && m[1][j+2]==2 && m[0][j+3]==0)
             return j+3;}
    return 10;
}

int bloccaaltradiagonalecaso1(char m[10][10])
{
    int i, j;
    
    for(i=4; i<10; i++)
    for(j=0; j<7; j++)
    {        
             if(m[i][j]==2 && m[i-1][j+1]==2 && m[i-2][j+2]==2 && m[i-3][j+3]==0 && m[i-4][j+3]!=0)
             return j+3;}
    return 10;
}

int bloccaaltradiagonalecaso2(char m[10][10])
{
    int i, j;
    
    for(i=0; i<7; i++)
    for(j=3; j<10; j++)
    {        
             if(m[i][j]==2 && m[i+1][j-1]==2 && m[i+2][j-2]==2 && m[i+3][j-3]==0 && m[i+2][j-3]!=0)
             return j-3;}
    return 10;
}

int bloccaaltradiagonalecaso34(char m[10][10])
{
    int i, j;
    
    for(i=0; i<7; i++)
    for(j=3; j<10; j++)
    {        
             if(m[i][j]==2 && m[i+1][j-1]==2 && m[i+3][j-3]==2 && m[i+2][j-2]==0 && m[i+1][j-2]!=0)
             return j-2;
             if(m[i][j]==2 && m[i+2][j-2]==2 && m[i+3][j-3]==2 && m[i+1][j-1]==0 && m[i][j-1]!=0)
             return j-1;}
    return 10;
}

int bloccaorizzontaledueriga1caso1(char m[10][10])
{
    int j;
    for(j=1; j<7; j++)
    {        if(m[0][j]==2 && m[0][j+1]==2 && m[0][j-1]==0 && m[0][j+2]==0 && m[0][j+3]==0)
             return j+2;}
    return 10;
}

int bloccaorizzontaleduecaso1(char m[10][10])
{
    int i, j;
    for(i=1; i<10; i++)
    for(j=1; j<7; j++)
    {        if(m[i][j]==2 && m[i][j+1]==2 && m[i][j-1]==0 && m[i][j+2]==0 && m[i][j+3]==0 && m[i-1][j-1]!=0 && m[i-1][j+2]!=0 && m[i-1][j+3]!=0)
             return j+2;}
    return 10;
}

int bloccaorizzontaledueriga1caso2(char m[10][10])
{
    int j;
    for(j=2; j<8; j++)
    {        if(m[0][j]==2 && m[0][j+1]==2 && m[0][j-1]==0 && m[0][j-2]==0 && m[0][j+2]==0)
             return j-1;}
    return 10;
}

int bloccaorizzontaleduecaso2(char m[10][10])
{
    int i, j;
    for(i=1; i<10; i++)
    for(j=2; j<8; j++)
    {        if(m[i][j]==2 && m[i][j+1]==2 && m[i][j-1]==0 && m[i][j-2]==0 && m[i][j+2]==0 && m[i-1][j-1]!=0 && m[i-1][j-2]!=0 && m[i-1][j+2]!=0)
             return j-1;}
    return 10;
}

int bloccaorizzontaledueriga1caso3(char m[10][10])
{
    int j;
    for(j=1; j<7; j++)
    {        if(m[0][j]==2 && m[0][j+2]==2 && m[0][j-1]==0 && m[0][j+1]==0 && m[0][j+3]==0)
             return j+1;}
    return 10;
}   

int bloccaorizzontaleduecaso3(char m[10][10])
{
    int i, j;
    for(i=1; i<10; i++)
    for(j=2; j<7; j++)
    {        if(m[i][j]==2 && m[i][j+2]==2 && m[i][j-1]==0 && m[i][j+1]==0 && m[i][j+3]==0 && m[i-1][j-1]!=0 && m[i-1][j+1]!=0 && m[i-1][j+3]!=0)
             return j+1;}
    return 10;
}


                 
             
             
