#include <stdio.h>

int vittoriaorizzontaleriga1caso1(char m[10][10])
{
    int j;
    
    for(j=0; j<7; j++)
    {        
             if(m[0][j]==1 && m[0][j+1]==1 && m[0][j+2]==1 && m[0][j+3]==0)
             return j+3;}
    return 10;
}

int vittoriaorizzontaleriga1caso2(char m[10][10])
{
    int j;
    
    for(j=1; j<8; j++)
    {        
             if(m[0][j]==1 && m[0][j+1]==1 && m[0][j+2]==1 && m[0][j-1]==0)
             return j-1;}
    return 10;
}

int vittoriaorizzontaleriga1caso34(char m[10][10])
{
    int j;
    
    for(j=0; j<7; j++)
    {        
             if(m[0][j]==1 && m[0][j+1]==1 && m[0][j+3]==1 && m[0][j+2]==0)
             return j+2;
             if(m[0][j]==1 && m[0][j+2]==1 && m[0][j+3]==1 && m[0][j+1]==0)
             return j+1;}
    return 10;
}

int vittoriaorizzontalecaso1(char m[10][10])
{
    int i, j;
    
    for(i=1; i<10; i++)
    for(j=0; j<7; j++)
    {        
             if(m[i][j]==1 && m[i][j+1]==1 && m[i][j+2]==1 && m[i][j+3]==0 && m[i-1][j+3]!=0)
             return j+3;}
    return 10;
}

int vittoriaorizzontalecaso2(char m[10][10])
{
    int i, j;
    
    for(i=1; i<10; i++)
    for(j=1; j<8; j++)
    {        
             if(m[i][j]==1 && m[i][j+1]==1 && m[i][j+2]==1 && m[i][j-1]==0 && m[i-1][j-1]!=0)
             return j-1;}
    return 10;
}

int vittoriaorizzontalecaso34(char m[10][10])
{
    int i, j;
    
    for(i=1; i<10; i++)
    for(j=0; j<7; j++)
    {        
             if(m[i][j]==1 && m[i][j+1]==1 && m[i][j+3]==1 && m[i][j+2]==0 && m[i-1][j+2]!=0)
             return j+2;
             if(m[i][j]==1 && m[i][j+2]==1 && m[i][j+3]==1 && m[i][j+1]==0 && m[i-1][j+1]!=0)
             return j+1;}
    return 10;
}

int vittoriaverticale(char m[10][10])
{
    int i, j;
    
    for(i=0; i<7; i++)
    for(j=0; j<10; j++)
    {        
             if(m[i][j]==1 && m[i+1][j]==1 && m[i+2][j]==1 && m[i+3][j]==0)
             return j;}
    return 10;
}

int vittoriadiagonalecaso1(char m[10][10])
{
    int i, j;
    
    for(i=0; i<7; i++)
    for(j=0; j<7; j++)
    {        
             if(m[i][j]==1 && m[i+1][j+1]==1 && m[i+2][j+2]==1 && m[i+3][j+3]==0 && m[i+2][j+3]!=0)
             return j+3;}
    return 10;
}

int vittoriadiagonalecaso2riga1(char m[10][10])
{
    int j;
    
    for(j=1; j<7; j++)
    {        
             if(m[1][j]==1 && m[2][j+1]==1 && m[3][j+2]==1 && m[0][j-1]==0)
             return j-1;}
    return 10;
}

int vittoriadiagonalecaso2(char m[10][10])
{
    int i, j;
    
    for(i=2; i<7; i++)
    for(j=1; j<7; j++)
    {        
             if(m[i][j]==1 && m[i+1][j+1]==1 && m[i+2][j+2]==1 && m[i-1][j-1]==0 && m[i-2][j-1]!=0)
             return j-1;}
    return 10;
}

int vittoriadiagonalecaso34(char m[10][10])
{
    int i, j;
    
    for(i=0; i<7; i++)
    for(j=0; j<7; j++)
    {        
             if(m[i][j]==1 && m[i+1][j+1]==1 && m[i+3][j+3]==1 && m[i+2][j+2]==0 && m[i+1][j+2]!=0)
             return j+2;
             if(m[i][j]==1 && m[i+2][j+2]==1 && m[i+3][j+3]==1 && m[i+1][j+1]==0 && m[i][j+1]!=0)
             return j+1;}
    return 10;
}
             
int vittoriaaltradiagonalecaso1riga1(char m[10][10])
{
    int j;
    
    for(j=0; j<7; j++)
    {        
             if(m[3][j]==1 && m[2][j+1]==1 && m[1][j+2]==1 && m[0][j+3]==0)
             return j+3;}
    return 10;
}

int vittoriaaltradiagonalecaso1(char m[10][10])
{
    int i, j;
    
    for(i=4; i<10; i++)
    for(j=0; j<7; j++)
    {        
             if(m[i][j]==1 && m[i-1][j+1]==1 && m[i-2][j+2]==1 && m[i-3][j+3]==0 && m[i-4][j+3]!=0)
             return j+3;}
    return 10;
}

int vittoriaaltradiagonalecaso2(char m[10][10])
{
    int i, j;
   
    for(i=0; i<7; i++)
    for(j=3; j<10; j++)
    {       
             if(m[i][j]==1 && m[i+1][j-1]==1 && m[i+2][j-2]==1 && m[i+3][j-3]==0 && m[i+2][j-3]!=0)
             return j-3;}
    return 10;
}

int vittoriaaltradiagonalecaso34(char m[10][10])
{
    int i, j;
 
    for(i=0; i<7; i++)
    for(j=3; j<10; j++)
    {      
             if(m[i][j]==1 && m[i+1][j-1]==1 && m[i+3][j-3]==1 && m[i+2][j-2]==0 && m[i+1][j-2]!=0)
             return j-2;
             if(m[i][j]==1 && m[i+2][j-2]==1 && m[i+3][j-3]==1 && m[i+1][j-1]==0 && m[i][j-1]!=0)
             return j-1;}
    return 10;
}


    
    
        
                 
             
             
