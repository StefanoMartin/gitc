#include "movimenti.c"

int clean_stdin();
void imponi(char m[10][10]);
void sceglinomigiocatori(char s1[20], char s2[20]);
int scegli(char m[10][10], char s1[20], char s2[20], int giocatore);
int sistema(int scelta, char m[10][10], int giocatore);
void stampa(char m[10][10]);
int vittoria(char m[10][10], char *s1, char *s2);
int altrapartita();
void annullarisultati(struct persona *giocatorea, struct persona *giocatoreb);
void salvarisultati(int fine, int minuti, double secondi, struct persona *giocatorea, struct persona *giocatoreb);
void istruzioni();
void highscore(struct persona *giocatorea, struct persona *giocatoreb);
void mostrahighscore();
int resethighscore();
