#include <stdlib.h>
#include "GF256.h"



int SBox(int Aij){ // SBox of AES
	int x[8], y[8], i, j;
	//printf("\n --- SB1 ---  %d", Aij);
	Aij = iGF256(Aij);
	//printf("\n --- SB2 --- ");
	for(i=0; i<8; i++){
		x[i] = Aij % 2;		Aij = (Aij - x[i])/2;
	}
	//printf("\n --- SB3 --- ");
	y[0] = (x[0] + 					   x[4] + x[5] + x[6] + x[7] + 1) % 2;
	y[1] = (x[0] + x[1] + 					  x[5] + x[6] + x[7] + 1) % 2;
	y[2] = (x[0] + x[1] + x[2] + 				   + x[6] + x[7]    ) % 2;
	y[3] = (x[0] + x[1] + x[2] + x[3] +						x[7]    ) % 2;
	y[4] = (x[0] + x[1] + x[2] + x[3] + x[4]  					    ) % 2;
	y[5] = (   	   x[1] + x[2] + x[3] + x[4] + x[5] +			   1) % 2;
	y[6] = (			  x[2] + x[3] + x[4] + x[5] + x[6] +       1) % 2;
	y[7] = (					 x[3] + x[4] + x[5] + x[6] + x[7]   ) % 2;
	//printf("\n --- SB4 --- ");
	int a = 1;
	for(j=0; j<8; j++){
		Aij = Aij + y[j]*a;		a = 2*a;
	}
	return Aij;
}

void ByteSub(int ncol, int A[4][ncol]){ // Application of SBox
	int i, j;
	for(j=0; j<ncol; j++){	for(i=0; i<4; i++){
		A[i][j] = SBox(A[i][j]);
	}}
}


void ShiftRows(int ncol, int A[4][ncol]){
	int i, temp0, temp1, temp2, temp3;
	
	temp0 = A[1][0];
	for(i=0; i<ncol-1; i++){ A[1][i] = A[1][i+1]; }
	A[1][ncol-1] = temp0;
	
	if(ncol != 8){
		temp0 = A[2][0];	temp1 = A[2][1];
		for(i=0; i<ncol-2; i++){ A[2][i] = A[2][i+2]; }
		A[2][ncol-2] = temp0;	A[2][ncol-1] = temp1;
		
		temp0 = A[3][0];	temp1 = A[3][1];	temp2 = A[3][2];
		for(i=0; i<ncol-3; i++){ A[3][i] = A[3][i+3]; }
		A[3][ncol-3] = temp0;	A[3][ncol-2] = temp1;	A[3][ncol-1] = temp2;	
	}else{
		temp0 = A[2][0];	temp1 = A[2][1];	temp2 = A[2][2];
		for(i=0; i<ncol-3; i++){ A[2][i] = A[2][i+3]; }
		A[2][ncol-3] = temp0;	A[2][ncol-2] = temp1;	A[2][ncol-1] = temp2;
		
		temp0 = A[3][0];	temp1 = A[3][1];	temp2 = A[3][2]; 	temp3 = A[3][3];
		for(i=0; i<ncol-4; i++){ A[3][i] = A[3][i+4]; }
		A[3][ncol-4] = temp0;	A[3][ncol-3] = temp1;	A[3][ncol-2] = temp2; 	A[3][ncol-1] = temp3;
	}
}

void MixColumn(int ncol, int A[4][ncol]){
	int b[4] = {2, 1, 1, 3};
	int i;
	for(i=0; i<ncol; i++){
		int d[4];
		int a[4] = {A[0][i],A[1][i],A[2][i],A[3][i]};
		pM(a, b, d);
		A[0][i]=d[0];	A[1][i]=d[1];
		A[2][i]=d[2];	A[3][i]=d[3];
	}
}

void AddRoundKey(int ncol, int nr, int A[4][ncol], int RK[nr+1][4][ncol], int r){
	int i,j;
	for(j=0; j<ncol; j++){ for(i=0; i<4; i++){
		A[i][j] ^= RK[r][i][j];
	}}
}
