#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "vittoria.h"
#include "movimenti.h"
#include "blocca.h"
#include "attacco.h"
#include "mossacasuale.h"
#include "cercavittoria.h"



int sceglicpu(char m[10][10])
{
    int ordine, scelta;
    for(scelta=10, ordine=0; ordine<100; ordine++)
    {
    if(ordine==0)
    {            scelta=vittoriaorizzontaleriga1caso1(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==1)
    {            scelta=vittoriaorizzontaleriga1caso2(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==2)
    {            scelta=vittoriaorizzontaleriga1caso34(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==3)
    {            scelta=vittoriaorizzontalecaso1(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==4)
    {            scelta=vittoriaorizzontalecaso2(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==5)
    {            scelta=vittoriaorizzontalecaso34(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==6)
    {            scelta=vittoriaverticale(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==7)
    {            scelta=vittoriadiagonalecaso1(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==8)
    {            scelta=vittoriadiagonalecaso2riga1(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==9)
    {            scelta=vittoriadiagonalecaso2(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==10)
    {            scelta=vittoriadiagonalecaso34(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==11)
    {            scelta=vittoriaaltradiagonalecaso1riga1(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==12)
    {            scelta=vittoriaaltradiagonalecaso1(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==13)
    {            scelta=vittoriaaltradiagonalecaso2(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==14)
    {            scelta=vittoriaaltradiagonalecaso34(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==15)
    {            scelta=bloccaorizzontaleriga1caso1(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
    
    if(ordine==16)
    {            scelta=bloccaorizzontaleriga1caso2(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==17)
    {            scelta=bloccaorizzontaleriga1caso34(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==18)
    {            scelta=bloccaorizzontalecaso1(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==19)
    {            scelta=bloccaorizzontalecaso2(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==20)
    {            scelta=bloccaorizzontalecaso34(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==21)
    {            scelta=bloccaverticale(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==22)
    {            scelta=bloccadiagonalecaso1(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==23)
    {            scelta=bloccadiagonalecaso2riga1(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==24)
    {            scelta=bloccadiagonalecaso2(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==25)
    {            scelta=bloccadiagonalecaso34(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==26)
    {            scelta=bloccaaltradiagonalecaso1riga1(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==27)
    {            scelta=bloccaaltradiagonalecaso1(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==28)
    {            scelta=bloccaaltradiagonalecaso2(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==29)
    {            scelta=bloccaaltradiagonalecaso34(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==30)
    {            scelta=bloccaorizzontaledueriga1caso1(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==31)
    {            scelta=bloccaorizzontaleduecaso1(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==32)
    {            scelta=bloccaorizzontaledueriga1caso2(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==33)
    {            scelta=bloccaorizzontaleduecaso2(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==34)
    {            scelta=bloccaorizzontaledueriga1caso3(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==35)
    {            scelta=bloccaorizzontaleduecaso3(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}        
       
    if(ordine==36)
    {            scelta=attaccoorizzontaledueriga1caso1(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==37)
    {            scelta=attaccoorizzontaleduecaso1(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==38)
    {            scelta=attaccoorizzontaledueriga1caso2(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==39)
    {            scelta=attaccoorizzontaleduecaso2(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==40)
    {            scelta=attaccoorizzontaledueriga1caso3(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==41)
    {            scelta=attaccoorizzontaleduecaso3(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                       
    if(ordine==42)
    {            scelta=cercavittoriaorizzontalediagonale1caso1(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}

    if(ordine==43)
    {            scelta=cercavittoriaorizzontalediagonale1caso2(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}

    if(ordine==44)
    {            scelta=cercavittoriaorizzontalediagonale1caso3(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}

    if(ordine==45)
    {            scelta=cercavittoriaorizzontalediagonale1caso4(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}

    if(ordine==46)
    {            scelta=cercavittoriaorizzontalealtradiagonale1caso1(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}

    if(ordine==47)
    {            scelta=cercavittoriaorizzontalealtradiagonale1caso2(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}

    if(ordine==48)
    {            scelta=cercavittoriaorizzontalealtradiagonale1caso3(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}

    if(ordine==49)
    {            scelta=cercavittoriaorizzontalealtradiagonale1caso4(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}

    if(ordine==50)
    {            scelta=cercavittoraorizzontalediagonale2caso1(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}

    if(ordine==51)
    {            scelta=cercavittoraorizzontalediagonale2caso2(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}

    if(ordine==52)
    {            scelta=cercavittoraorizzontalediagonale2caso3(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}

    if(ordine==53)
    {            scelta=cercavittoraorizzontalealtradiagonale2caso1(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}

    if(ordine==54)
    {            scelta=cercavittoraorizzontalealtradiagonale2caso2(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==55)
    {            scelta=cercavittoraorizzontalealtradiagonale2caso3(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}
                 
    if(ordine==56)
    {            scelta=cercavittoriaorizzontalediagonale3caso1(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}       

    if(ordine==57)
    {            scelta=cercavittoriaorizzontalediagonale3caso2(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}

    if(ordine==58)
    {            scelta=cercavittoriaorizzontalediagonale3caso3(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}

    if(ordine==59)
    {            scelta=cercavittoriaorizzontalediagonale3caso4(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}

    if(ordine==60)
    {            scelta=cercavittoriaorizzontalediagonale3caso5(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}

    if(ordine==61)
    {            scelta=cercavittoriaorizzontalediagonale3caso6(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}

    if(ordine==62)
    {            scelta=cercavittoriaorizzontalealtradiagonale3caso1(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}

    if(ordine==63)
    {            scelta=cercavittoriaorizzontalealtradiagonale3caso2(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}

    if(ordine==64)
    {            scelta=cercavittoriaorizzontalealtradiagonale3caso3(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}

    if(ordine==65)
    {            scelta=cercavittoriaorizzontalealtradiagonale3caso4(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}

    if(ordine==66)
    {            scelta=cercavittoriaorizzontalealtradiagonale3caso5(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}

    if(ordine==67)
    {            scelta=cercavittoriaorizzontalealtradiagonale3caso6(m);
                 if(scelta!=10)
                 {//printf("%d", ordine); 
		return scelta;}}

    if(ordine==68)
    {             scelta=mossacasuale(m);
                  if(scelta!=10)
                  {//printf("%d", ordine); 
		return scelta;}}     
     
    if(ordine==69)
    {            
                 //printf("%d", ordine); 
                 return scelta=rand()%10;}
    }
}
    


int main()
{
    int scelta, fine, turno, ok, decisione, esci=0, minuti, altrapar;
    char c;
    char m[10][10];
    struct persona giocatorea;
    struct persona giocatoreb;
    double secondi;
    clock_t time1, time2;
    srand((unsigned)time(NULL));
    printf("Welcome in Connect Four!\n");
    printf("Created by Stefano Martin.\n");
    
    do
    {     printf("\n\n1. Player 1 VS Player 2");
          printf("\n\n2. Player VS CPU");
          printf("\n\n3. Instructions");
          printf("\n\n4. Chronology");
          printf("\n\n5. Quit");
          printf("\n\nWhat do you want to do? ");
          
    	  if(((scanf("%d%c", &decisione, &c)!=2 || c!='\n') && clean_stdin()) && decisione!=(1 || 2 || 3 || 4 || 5))
          {
                    printf("Select a value between 1 and 5.\n");
          }
          if(decisione==1)
          {               printf("\n");
                          sceglinomigiocatori(giocatorea.nome, giocatoreb.nome);
                          annullarisultati(&giocatorea, &giocatoreb);
                          do
                          {imponi(m);
                           stampa(m);
                           time1=clock();
                           for(turno=0, fine=0; turno<100, fine==0; turno++)
                           {
                                     scelta=scegli(m, giocatorea.nome, giocatoreb.nome, turno);
                                     sistema(scelta, m, turno);
                                     stampa(m);
                                     fine=vittoria(m, giocatorea.nome, giocatoreb.nome);
                                     }
                           if(turno==100)
                           printf("\nThe result of the match is draw");
                           printf("\n");
                           time2=clock();
                           secondi=((double)(time2-time1))/CLK_TCK;
                           minuti=secondi/60;
                           secondi=secondi-60*minuti;
                           printf("The match lasted %d minutes and %g seconds.\n", minuti, secondi);
                           salvarisultati(fine, minuti, secondi, &giocatorea, &giocatoreb);
                           altrapar=altrapartita();
                           }while(altrapar==0);
                           highscore(&giocatorea, &giocatoreb);
                           }
    
          if(decisione==2)
          {               printf("\n");
                          printf("NB: The first name is the one of the CPU.");
                          sceglinomigiocatori(giocatorea.nome, giocatoreb.nome);
                          annullarisultati(&giocatorea, &giocatoreb);
                          do
                          {
                           imponi(m);
                           stampa(m);
                           time1=clock();
                           for(turno=0, fine=0; turno<100, fine==0; turno++)
                           {
                                     if(turno%2==0)
                                     {do
                                      {
                                      scelta=sceglicpu(m);
                                      if(m[9][scelta]!=0)
                                      ok=0;
                                      else
                                      ok=1;
                                      } while(ok==0);
                                     printf("\nPc %s inserts its coin in %d.\n", giocatorea.nome, scelta+1);
                                     }
                                     else
                                     scelta=scegli(m, giocatorea.nome, giocatoreb.nome, turno);
                                     sistema(scelta, m, turno);
                                     stampa(m);
                                     fine=vittoria(m, giocatorea.nome, giocatoreb.nome);
                           }
                           if(turno==100)
                           printf("The result of the match is draw");
                           printf("\n");
                           time2=clock();
                           secondi=((double)(time2-time1))/CLK_TCK;
                           minuti=secondi/60;
                           secondi=secondi-60*minuti;
                           printf("The match lasted %d minutes and %g seconds.\n", minuti, secondi);
                           salvarisultati(fine, minuti, secondi, &giocatorea, &giocatoreb);
                           altrapar=altrapartita();
                           }while(altrapar==0);
                           highscore(&giocatorea, &giocatoreb);
          }
                     
          if(decisione==3)
          {               istruzioni();
          }
          
          if(decisione==4)
          {               mostrahighscore();
                          resethighscore();
          }
          
          if(decisione==5)
          {               printf("Bye bye!");
                          esci=1;
          }
          
          
    }while(esci==0);
    return 0;
}
                  

