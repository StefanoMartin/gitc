#include <stdio.h>
#include <string.h>
#include <time.h>

struct persona
{
       int vittoria;
       int partitefatte;
       int minuti;
       double secondi;
       char nome[20];
};

int clean_stdin()
{
    while (getchar()!='\n');
    return 1;
}

// Con "imponi" faccio in maniera che la matrice sia completamente formata da soli spazi
void imponi(char m[10][10])
{
    int i,j;
    for(i=0; i<10; i++)
    for(j=0; j<10; j++)
    m[i][j]=0;
}

// La funzione "sceglinomegiocatori" serve a fare in modo che i giocatori (umani o pc) digitino il loro nome.
void sceglinomigiocatori(char s1[20], char s2[20])
{
     printf("\nSelect the name of the first player: ");
     scanf("%s", s1);     
     printf("\nSelect the name of the second player: ");
     scanf("%s", s2);
}



// "Scegli" serve all'utente per scegliere la propria colonna; essa verifica anche se le colonne sono gi� piene o meno o se il valore dato � errato
int scegli(char m[10][10], char s1[20], char s2[20], int giocatore)
{
     int scelta, ok;
     char c;
     do
     { 
          if(giocatore%2==0)
          printf("\n%s, in which column do you want to insert your coin? ", s1);
          else
          printf("\n%s, in which column do you want to insert your coin? ", s2);
          ok=1;
          if(((scanf("%d%c", &scelta, &c)!=2 || c!='\n') && clean_stdin()) || scelta>10 || scelta==0)
          {
                       printf("\n");
					   printf("Your value is not correct. Select another one.\n");
                       ok=0;
                       }
          else
          if(m[9][scelta-1]!=0)
          {
                       printf("The column is full. Select another column.\n");
                       ok=0;
                       }
     }while(ok==0);
     return (scelta-1);
}

/* "Sistema" serve per mettere nella casella un coin di un giocatore o dell'altro; il turno 
viene deciso in relazione se giocatore � un valore pari o dispari.*/
int sistema(int scelta, char m[10][10], int giocatore)
{
    int i;
    for(i=0; i<10; i++)
    if(m[i][scelta]==0)
    {
                       if(giocatore%2==0)
                       m[i][scelta]=1;
                       else
                       m[i][scelta]=2;
                       return 0;}
    return 0;
}

//"Stampa" serve per mostrare il tabellone di gioco    
void stampa(char m[10][10])
{
     int i, j;
     printf("\n\t");
     for(i=9; i>=0; i--)
     {
              for(j=0; j<10; j++)
              printf(" %c", m[i][j]);
              printf("\n\t");
              }
     printf(" 1 2 3 4 5 6 7 8 9 10 \n");
}

//"Vittoria" mostra le 4 possibilit� di vittorie: verticale, orizzontale, diagonale (nelle due direzioni)
int vittoria(char m[10][10], char s1[20], char s2[20])
{
    int i, j;
    for(i=0; i<7; i++)
    for(j=0; j<10; j++)
    if((m[i][j]==1 || m[i][j]==2) && m[i][j]==m[i+1][j] && m[i+1][j]==m[i+2][j] && m[i+2][j]==m[i+3][j])
    {if(m[i][j]==1)
    {printf("\n%s won the match!", s1);
    return 1;}
    else
    {printf("\n%s won the match!", s2);
    return 2;}
    }
    
    for(i=0; i<10; i++)
    for(j=0; j<7; j++)
    if((m[i][j]==1 || m[i][j]==2) && m[i][j]==m[i][j+1] && m[i][j+1]==m[i][j+2] && m[i][j+2]==m[i][j+3])
    {if(m[i][j]==1)
    {printf("\n%s won the match!", s1);
    return 1;}
    else
    {printf("\n%s won the match!", s2);
    return 2;}
    }
    
    for(i=0; i<7; i++)
    for(j=0; j<7; j++)
    if((m[i][j]==1 || m[i][j]==2) && m[i][j]==m[i+1][j+1] && m[i+1][j+1]==m[i+2][j+2] && m[i+2][j+2]==m[i+3][j+3])
    {if(m[i][j]==1)
    {printf("\n%s won the match!", s1);
    return 1;}
    else
    {printf("\n%s won the match!", s2);
    return 2;}
    }
    
    for(i=3; i<10; i++)
    for(j=0; j<7; j++)
    if((m[i][j]==1 || m[i][j]==2) && m[i][j]==m[i-1][j+1] && m[i-1][j+1]==m[i-2][j+2] && m[i-2][j+2]==m[i-3][j+3])
    {if(m[i][j]==1)
    {printf("\n%s won the match!", s1);
    return 1;}
    else
    {printf("\n%s won the match!", s2);
    return 2;}
    }
    
    return 0;
}

// La funzione "altrapartita" serve per chiedere ai giocatori se vogliono (o meno) fare un'altra partita con gli stessi giocatori.
int altrapartita()
{
     char scelta, u;
     do
     { //scanf("%c", &u); 
       printf("\nDo you want to do another match with these players (Y/N)? ");
       scanf("%c", &scelta);
       if(scelta=='Y' || scelta=='y')
       return 0;
       if(scelta=='N' || scelta=='n')
       return 1;
       printf("\nInvalid command!");
     } while(scelta!=('Y' || 'y' || 'n' || 'N'));
}

// "Annullarisultati" serve per rimettere la statistiche dei giocatori a 0.
void annullarisultati(struct persona *giocatorea, struct persona *giocatoreb)
{
     giocatorea->vittoria=0;
     giocatoreb->vittoria=0;
     giocatorea->partitefatte=0;
     giocatorea->minuti=0;
     giocatorea->secondi=(double)0;
}

// "Salvarisultati" si avvia quando si conclude un match; essa in realt� NON salva i risultati ma li mostra al giocatore e li assegna a delle variabili.
void salvarisultati(int fine, int minuti, double secondi, struct persona *giocatorea, struct persona *giocatoreb)
{
     int tmp;

     if(fine==1)
     {giocatorea->vittoria=giocatorea->vittoria+1;
     }

     if(fine==2)
     {giocatoreb->vittoria=giocatoreb->vittoria+1;
     }

     giocatorea->partitefatte=giocatorea->partitefatte+1;
     giocatorea->minuti=giocatorea->minuti+minuti;
     giocatorea->secondi=giocatorea->secondi+secondi;
     tmp=giocatorea->secondi/60;
     giocatorea->minuti=giocatorea->minuti+tmp;
     giocatorea->secondi=giocatorea->secondi-((double)(tmp)*60);
     printf("\nRESULTS MATCH DONE\n");
     printf("%s %d - %s %d\n", giocatorea->nome, giocatorea->vittoria, giocatoreb->nome, giocatoreb->vittoria);
     printf("You did %d matches. You played for %d minutes and %g seconds", giocatorea->partitefatte, giocatorea->minuti, giocatorea->secondi);
     printf("\n");
}
    
// "Istruzioni" � una serie di "printf" che spiega rapidamente come si gioca a Forza quattro.    
void istruzioni()
{
     char continua;
     printf("\nISTRUCTION OF CONNECT FOUR!"); 
     printf("\nHOW TO PLAY"); 
     printf("\nIt is a turn game. Player 1 begins, then it is the turn of Player 2."); 
     printf("\nAfter it is again turn for Player 1 and so on, until one player wins\n or a draw is obtained.");
     printf("\nThe game field is a table with 100 entries, 10 rows and 10 columns."); 
     printf("\nIn each turn the player needs to select in which column to insert his coin."); 
     printf("\nThe coin will be insert at the lowest level available of the choiced column."); 
     printf("\nIt the column is full, the player needs to select another column."); 
     printf("\n\nPress enter to continue."); 
     scanf("%c", &continua);
     printf("\nOBJECTIVE OF THE GAME"); 
     printf("\nThe objective of the game is to insert inline at least four of your coins."); 
     printf("\nFor \"inline\" we mean that your coins needs to be near either horizontally,"); 
     printf("\nvertically, or diagonally."); 
     printf("\nBe careful to not give the victory to your adversary."); 
     printf("\nIf you fill all the table without a winner, then a draw result is obtained."); 
     printf("\n\nPress enter to continue."); 
     scanf("%c", &continua);
     printf("\nBE CAREFUL!"); 
     printf("\nThe result draw is not added in the statistics."); 
     printf("\nThe names cannot be longer than 20 characters."); 
     printf("\n\nPress enter to conclude this explanation."); 
     scanf("%c", &continua);
}

// "highscore" � la vera funzione che salva le statistiche delle varie partite; essa richiama il file "highscore.txt" e salva semplicemente le statistiche tramite il comando "fprintf". Essa scrive sul file senza sovrascrivere le stringhe gi� esistenti in esso.
void highscore(struct persona *giocatorea, struct persona *giocatoreb)
{
     FILE* scrivi;
     time_t data;
     struct tm *tempo;
     time(&data);
     tempo=localtime(&data);
     
     scrivi=fopen("highscore.txt", "a");
     fprintf(scrivi, "\n");
     fprintf(scrivi, "%s", asctime(tempo));
     fprintf(scrivi, "%s %d - %s %d ", giocatorea->nome, giocatorea->vittoria, giocatoreb->nome, giocatoreb->vittoria);
     fprintf(scrivi, "You did %d matches. You played for %d minutes and %g seconds ", giocatorea->partitefatte, giocatorea->minuti, giocatorea->secondi);
     fclose(scrivi);
}

// Tramite "fscanf", mostrahighscore assegna alla matrice p[1000][20] le stringhe contenute nel file "highscore.txt" e le mostra su schermo. 
void mostrahighscore()
{
     int flag=1;
     int i, k, numerorighe;
     int j=9, n1, n2, alterna;
     char tmp;
     char p[1000][20];
     printf("\n");
     FILE* leggi;
     leggi=fopen("highscore.txt", "r");
     for(i=0; flag; i++)
     {        tmp=fscanf(leggi, "%s", p[i]);
              if(tmp==EOF)
              flag=0;}
     numerorighe=i;
     fclose(leggi);
     for(k=0, j=0, n1=0, n2=0, alterna=0; k<numerorighe; k++, j++)
     {printf("%s", p[k]);
     printf(" ");
     if(j==9+n1*10+n2*12)
     {       alterna++;
             if(alterna%2==0)
             {n1++;
             printf("\n\n");}
             else
             {n2++;
             printf("\n");}
             }}
  
     
}

// Opposta a "highscore", essa sovrascrive il file cancellando l'intero contenuto del file "highscore.txt".
int resethighscore()
{
     char scelta;
     FILE* cancella;
     do{
           printf("\nDo you want to reset the chronology? (Y/N) ");
           scanf("%c", &scelta);
           if(scelta=='Y' || scelta=='y')
           {        cancella=fopen("highscore.txt", "w");
                    fprintf(cancella, "");
                    fclose(cancella);
                    return 0;}      
           if(scelta=='N' || scelta=='n')
           {        return 0;}
           printf("\nInvalid command!");
           }while(scelta!=('Y' || 'y' || 'n' || 'N'));
}
