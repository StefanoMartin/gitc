#ifndef CHECK2
#define CHECK2

#include"oAES.c"

int SBox(int Aij);
void ByteSub(int ncol, int A[4][ncol]);
void ShiftRows(int ncol, int A[4][ncol]);
void MixColumn(int ncol, int A[4][ncol]);
void AddRoundKey(int ncol, int nr, int A[4][ncol], int RK[nr+1][4][ncol], int r);

#endif
