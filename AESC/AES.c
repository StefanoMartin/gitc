#include <stdio.h>
#include <stdlib.h>
#include "oEncAES.h"

int clean_stdin(){
    while (getchar()!='\n');
    return 1;
}


void Encoding(int n, int nk, int enc, char password[100]){
	FILE *fp, *fw;
	int ncol = n/32, nr = 14;
	int i, j;
	
	if(ncol == 4 && nk == 4){ nr = 10; }
	else if((ncol == 4 && nk == 6) || (ncol == 6 && (nk == 4 || nk == 6))){
		nr = 12; 
	}
	
	int A[4][ncol], RK[nr+1][4][ncol];
	KeySchedule(ncol, nk, nr, password, RK);
	
	char namefile[20], outputfile[20];
	unsigned char *buffer;
	
	while(1==1){
		printf("\nName file in input: ");
		scanf("%s", namefile);
		fp = fopen(namefile,"rb"); 
		if(fp == NULL){
			printf("\nError: File cannot be opened.\n");
			printf("\nSelect another file\n.");
		}else
			break;
	}
	
	printf("\nName file in output: ");
	scanf("%s", outputfile);
	fw = fopen(outputfile,"wb");

	if((buffer=malloc(4*ncol*sizeof(char))) == NULL){
        printf("Error: No enough memory\n");
        getchar();
        exit(-1);
    }
	
	while(fread(buffer,sizeof(char),4*ncol,fp) > 0){
		for(i=0; i<ncol; i++){
			for(j=0; j<4; j++){
				A[j][i] = *(buffer + 4*i+j);
				//printf("%d ", A[j][i]);
		}}
		
		if(enc==1)
			encRij(ncol, nr, A, RK);
		else
			decRij(ncol, nr, A, RK);
			
		for(i=0; i<ncol; i++){
			for(j=0; j<4; j++){
				*(buffer + 4*i+j) = A[j][i];
		}}
		
		fwrite(buffer,sizeof(char),4*ncol,fw);
	}
	
	if(enc==1)
		printf("Encryption complete!");
	else
		printf("Decryption complete!");	
		
	free(buffer);
	fclose(fp); fclose(fw);
		
}

int main(){
    int a = 4, n=128, nk=4, enc=1, exit = 1;
	char password[100], c;
	printf("Thank to use this small program.");
	printf("\nYou can encrypt with AES-128, AES-192 or AES-256.");
	printf("\nThis program was created by Stefano Martin.");
	
	while(2==2){
		while(1==1){
			printf("\nInsert your password: ");
			scanf("%s", password);
			if(password[0] == ' '){
				printf("Please insert a password.\n");
			}else
				break;	
		}
		
		
		printf("\nWhich security do you want?");
		printf("\nPlease select one of these options.");
		while(1==1){
			printf("\n1) AES-128, 2) AES-192, 3) AES-256\n\n");
			if(((scanf("%d%c", &a, &c)!=2 || c!='\n') && clean_stdin()) && a !=(1 || 2 || 3)){
				printf("\nPlease select one of the available options."); }
			else if(a == 1){ n = 128; break; }
			else if(a == 2){ n = 192; break; }
			else if(a == 3){ n = 256; break; }
		}
	
		a=4;
		printf("\nHow big do you want your key?");
		printf("\nPlease select one of these options.");
		while(1==1){
			printf("\n1) 4, 2) 6, 3) 8\n\n");
			if(((scanf("%d%c", &a, &c)!=2 || c!='\n') && clean_stdin()) && a !=(1 || 2 || 3)){
				printf("\nPlease select one of the available options."); }
			else if(a == 1){ nk = 4; break; }
			else if(a == 2){ nk = 6; break; }
			else if(a == 3){ nk = 8; break; }
		}
		
		a=4;
		printf("\nDo you want to encrypt or decrypt?");
		while(1==1){
			printf("\n1) Encrypt, 2) Decrypt\n\n");
			if(((scanf("%d%c", &a, &c)!=2 || c!='\n') && clean_stdin()) && a !=(1 || 2)){
				printf("\nPlease select one of the available options."); }
			else if(a == 1){ enc = 1; break; }
			else if(a == 2){ enc = 2; break; }
		}
		
		Encoding(n, nk, enc, password);
		
		a=4;
		printf("\n\nDo you want to use again this program?");
		while(1==1){
			printf("\n1) Yes, 2) No\n\n");
			if(((scanf("%d%c", &a, &c)!=2 || c!='\n') && clean_stdin()) && a !=(1 || 2)){
				printf("\nPlease select one of the available options."); }
			if(a == 1){ exit = 1; break; }
			if(a == 2){ exit = 0; break; }
		}
		
		if(exit == 0){ break; }
	}
	
	return 0;
}
